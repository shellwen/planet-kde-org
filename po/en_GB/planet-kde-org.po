#
# Steve Allewell <steve.allewell@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2021-07-31 16:38+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.3\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planet KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "Planet KDE site providing newest news from the KDE Project"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Add your own feed"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Welcome to Planet KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Go down one post"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Go up one post"
